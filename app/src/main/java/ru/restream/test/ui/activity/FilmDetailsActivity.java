package ru.restream.test.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;

import ru.restream.test.R;
import ru.restream.test.net.WebApiService;
import ru.restream.test.net.WebService;
import ru.restream.test.net.dto.Film;
import ru.restream.test.net.dto.Genre;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by HP on 28.06.2016.
 */
public class FilmDetailsActivity extends AppCompatActivity {

    private final String TAG = FilmDetailsActivity.class.getSimpleName();
    private static final String EXTRA_KEY_FILM = "film";

    private ImageView imageLogo;
    private TextView textTitle;
    private TextView textTitleOriginal;
    private TextView textYear;
    private TextView textVoteAverage;
    private TextView textPopularity;
    private TextView textVoteCount;
    private TextView textGenres;
    private TextView textOverview;
    private View viewTrailer;
    private ImageView imagePreview;

    private WebApiService webApiService = new WebService();
    private Subscription videoPreviewSubscription;
    private Film film;

    public static void start(Context context, Film film) {
        Intent starter = new Intent(context, FilmDetailsActivity.class);
        starter.putExtra(EXTRA_KEY_FILM, film);
        context.startActivity(starter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        film = (Film) getIntent().getSerializableExtra(EXTRA_KEY_FILM);
        if (film == null) {
            finish();
            return;
        }
        setContentView(R.layout.activity_film_details);
        if (getActionBar() != null)
            getActionBar().setDisplayHomeAsUpEnabled(true);

        bindView();
        putFilm(film);
        viewTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayVideoActivity.start(FilmDetailsActivity.this, film.video);
            }
        });
    }

    private void bindView() {
        imageLogo = (ImageView) findViewById(R.id.image_logo);
        textTitle = (TextView) findViewById(R.id.text_title);
        textTitleOriginal = (TextView) findViewById(R.id.text_title_original);
        textYear = (TextView) findViewById(R.id.text_year);
        textVoteAverage = (TextView) findViewById(R.id.text_vote_average);
        textPopularity = (TextView) findViewById(R.id.text_popularity);
        textVoteCount = (TextView) findViewById(R.id.text_vote_count);
        textGenres = (TextView) findViewById(R.id.text_genres);
        textOverview = (TextView) findViewById(R.id.text_overview);
        viewTrailer = findViewById(R.id.view_trailer);
        imagePreview = (ImageView) findViewById(R.id.image_preview);
    }

    private void putFilm(Film film) {
        Picasso.with(this)
                .load(film.poster_path)
                .error(R.drawable.ic_cloud_off_gray_100dp)
                .into(imageLogo);
        textTitle.setText(film.title);
        textTitleOriginal.setText(film.original_title);

        if (film.release_date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(film.release_date);
            textYear.setText(String.valueOf(calendar.get(Calendar.YEAR)));
        }
        textVoteAverage.setText(String.valueOf(film.vote_average));
        textPopularity.setText(getString(R.string._popularity, film.popularity));
        textVoteCount.setText(getString(
                film.vote_count == 1 ? R.string._vote : R.string._votes,
                film.vote_count
        ));
        textGenres.setText(convertGenresToString(film.genre_ids));
        textOverview.setText(film.overview);

        setImagePreview(film.video);
    }

    private String convertGenresToString(List<Genre> genres) {
        if (genres == null || genres.isEmpty())
            return null;

        StringBuilder builder = new StringBuilder();
        for (Genre item : genres) {
            builder.append(item.name);
            builder.append(", ");
        }

        return builder.substring(0, builder.length() - 2);
    }

    private void setImagePreview(String videoUrl) {
        if (videoUrl == null || videoUrl.isEmpty()) {
            viewTrailer.setVisibility(View.GONE);
            return;
        }

        videoPreviewSubscription = webApiService.getVideoPreview(film.video)
                .subscribe(new Action1<Bitmap>() {
                    @Override
                    public void call(final Bitmap bitmap) {
                        if (bitmap == null)
                            return;

                        imagePreview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                if (imagePreview.getHeight() > 0) {
                                    imagePreview.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                    Bitmap scaledBitmap = scale(bitmap, imagePreview.getHeight());
                                    if (scaledBitmap != null)
                                        imagePreview.setImageDrawable(new BitmapDrawable(getResources(), scaledBitmap));
                                }
                            }
                        });
                    }
                });
    }

    public Bitmap scale(Bitmap bitmap, int newWidth) {
        int newHeight = bitmap.getHeight() * (newWidth / bitmap.getWidth());
        try {
            return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
        } catch (Exception e) {
            Log.e(TAG, "Cannot scale bitmap", e);
            return null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (videoPreviewSubscription != null)
            videoPreviewSubscription.unsubscribe();
    }
}
