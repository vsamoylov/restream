package ru.restream.test.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import ru.restream.test.R;
import ru.restream.test.net.WebApiService;
import ru.restream.test.net.WebService;
import ru.restream.test.net.dto.Film;
import ru.restream.test.net.dto.FilmsResponse;
import ru.restream.test.ui.adapter.FilmsAdapter;
import rx.Subscription;
import rx.functions.Action1;

public class ListFilmsActivity extends AppCompatActivity implements FilmsAdapter.OnItemClickListener<Film> {

    private RecyclerView recycler;
    private View viewProgress;
    private View viewEmpty;
    private View viewError;

    private WebApiService webApiService = new WebService();
    private Subscription filmsSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_films);
        bindView();

        viewError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeGetFilmsRequest();
            }
        });

        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setHasFixedSize(true);

        executeGetFilmsRequest();
    }

    private void bindView() {
        recycler = (RecyclerView) findViewById(R.id.recycler);
        viewProgress = findViewById(R.id.view_progress);
        viewEmpty = findViewById(R.id.view_empty);
        viewError = findViewById(R.id.view_error);
    }

    private void executeGetFilmsRequest() {
        showView(ViewType.PROGRESS);

        filmsSubscription = webApiService.getFilmsObservable()
                .subscribe(new Action1<FilmsResponse>() {
                    @Override
                    public void call(FilmsResponse filmsResponse) {
                        putFilms(filmsResponse.results);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        showView(ViewType.ERROR);
                    }
                });
    }

    private void putFilms(List<Film> films) {
        if (films == null || films.isEmpty()) {
            showView(ViewType.EMPTY);
            return;
        }
        FilmsAdapter filmsAdapter = new FilmsAdapter(films);
        filmsAdapter.setOnItemClickListener(this);
        if (recycler.getAdapter() != null)
            recycler.swapAdapter(filmsAdapter, false);
        else
            recycler.setAdapter(filmsAdapter);
        showView(ViewType.LIST);
    }

    private void showView(ViewType viewType) {
        recycler.setVisibility(viewType == ViewType.LIST ? View.VISIBLE : View.GONE);
        viewProgress.setVisibility(viewType == ViewType.PROGRESS ? View.VISIBLE : View.GONE);
        viewEmpty.setVisibility(viewType == ViewType.EMPTY ? View.VISIBLE : View.GONE);
        viewError.setVisibility(viewType == ViewType.ERROR ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(int position, Film item) {
        FilmDetailsActivity.start(this, item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (filmsSubscription != null)
            filmsSubscription.unsubscribe();
    }

    private enum ViewType {
        LIST, PROGRESS, EMPTY, ERROR
    }
}
