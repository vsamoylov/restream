package ru.restream.test.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import ru.restream.test.R;

/**
 * Created by HP on 30.06.2016.
 */
public class PlayVideoActivity extends AppCompatActivity {

    private final String TAG = PlayVideoActivity.class.getSimpleName();
    private static final String EXTRA_KEY_VIDEO_URL = "video_url";

    private VideoView video;
    private ImageView imagePlay;
    private ProgressBar progress;
    private View viewError;

    private String videoUrl;

    public static void start(Context context, String videoUrl) {
        Intent starter = new Intent(context, PlayVideoActivity.class);
        starter.putExtra(EXTRA_KEY_VIDEO_URL, videoUrl);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        videoUrl = getIntent().getStringExtra(EXTRA_KEY_VIDEO_URL);
        if (videoUrl == null) {
            finish();
            return;
        }

        setContentView(R.layout.activity_play_video);
        bindView();

        imagePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPlayClick();
            }
        });
        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                imagePlay.setVisibility(View.VISIBLE);
            }
        });
        video.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                viewError.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                return true;
            }
        });
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progress.setVisibility(View.GONE);

            }
        });

        try {
            MediaController mediaController = new MediaController(this);
            mediaController.setAnchorView(video);
            mediaController.setMediaPlayer(video);
            Uri uri = Uri.parse(videoUrl);
            video.setMediaController(mediaController);
            video.setVideoURI(uri);
            onPlayClick();
        } catch (Exception e) {
            Log.e(TAG, "Error on initializing VideoView", e);
            viewError.setVisibility(View.VISIBLE);
        }
    }

    private void bindView() {
        video = (VideoView) findViewById(R.id.video);
        imagePlay = (ImageView) findViewById(R.id.image_play);
        progress = (ProgressBar) findViewById(R.id.progress);
        viewError = findViewById(R.id.view_error);
    }

    private void onPlayClick() {
        imagePlay.setVisibility(View.GONE);
        video.requestFocus();
        video.start();
    }
}
