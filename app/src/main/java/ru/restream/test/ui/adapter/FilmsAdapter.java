package ru.restream.test.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;

import ru.restream.test.R;
import ru.restream.test.net.dto.Film;

/**
 * Created by HP on 28.06.2016.
 */
public class FilmsAdapter extends RecyclerView.Adapter<FilmsAdapter.ViewHolder> {

    private final List<Film> films;
    private OnItemClickListener<Film> onItemClickListener;

    public FilmsAdapter(List<Film> films) {
        this.films = films;
    }

    public void setOnItemClickListener(OnItemClickListener<Film> listener) {
        this.onItemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_film, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Film film = films.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null)
                    onItemClickListener.onClick(holder.getAdapterPosition(), film);
            }
        });
        Picasso.with(holder.itemView.getContext())
                .load(film.poster_path)
                .error(R.drawable.ic_cloud_off_gray_100dp)
                .into(holder.imageLogo);
        holder.textTitle.setText(film.title);
        holder.textTitleOriginal.setText(film.original_title);

        if (film.release_date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(film.release_date);
            holder.textYear.setText(String.valueOf(calendar.get(Calendar.YEAR)));
        }
        holder.textVoteAverage.setText(String.valueOf(film.vote_average));
    }

    @Override
    public int getItemCount() {
        return films.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageLogo;
        public TextView textTitle;
        public TextView textTitleOriginal;
        public TextView textYear;
        public TextView textVoteAverage;

        public ViewHolder(View itemView) {
            super(itemView);
            imageLogo = (ImageView) itemView.findViewById(R.id.image_logo);
            textTitle = (TextView) itemView.findViewById(R.id.text_title);
            textTitleOriginal = (TextView) itemView.findViewById(R.id.text_title_original);
            textYear = (TextView) itemView.findViewById(R.id.text_year);
            textVoteAverage = (TextView) itemView.findViewById(R.id.text_vote_average);
        }
    }

    public interface OnItemClickListener<T> {
        void onClick(int position, T item);
    }
}
