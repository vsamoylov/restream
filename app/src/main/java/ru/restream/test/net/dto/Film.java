package ru.restream.test.net.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by HP on 27.06.2016.
 */
public class Film implements Serializable{

    public boolean adult;
    public List<Genre> genre_ids;
    public String id;
    public String original_language;
    public String original_title;
    public String overview;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    public Date release_date;
    public String poster_path;
    public float popularity;
    public String title;
    public String video;
    public float vote_average;
    public int vote_count;

    public void setGenres(List<Genre> genres) {
        this.genre_ids = genres;
    }
}
