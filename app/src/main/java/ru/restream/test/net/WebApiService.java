package ru.restream.test.net;

import android.graphics.Bitmap;

import ru.restream.test.net.dto.FilmsResponse;
import rx.Observable;

/**
 * Created by HP on 27.06.2016.
 */
public interface WebApiService {

    Observable<FilmsResponse> getFilmsObservable();
    Observable<Bitmap> getVideoPreview(String videoPath);
}
