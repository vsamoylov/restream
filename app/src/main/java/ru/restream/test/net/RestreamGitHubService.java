package ru.restream.test.net;

import retrofit2.http.GET;
import ru.restream.test.net.dto.FilmsResponse;
import rx.Observable;

/**
 * Created by HP on 27.06.2016.
 */
public interface RestreamGitHubService {

    @GET("numbata/5ed307d7953c3f7e716f/raw/637c9df9a252a1127a6569adb2b8486a8e559682/movies.json")
    Observable<FilmsResponse> getFilms();
}
