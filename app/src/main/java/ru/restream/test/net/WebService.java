package ru.restream.test.net;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.util.Log;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import ru.restream.test.net.dto.FilmsResponse;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by HP on 27.06.2016.
 * <p/>
 * Implementation of web api based on static file from GitHub with Retrofit
 */
public class WebService implements WebApiService {

    private final String TAG = WebService.class.getSimpleName();
    private final String MAIN_URL = "https://gist.githubusercontent.com";

    private final RestreamGitHubService service;

    public WebService() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MAIN_URL)
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        service = retrofit.create(RestreamGitHubService.class);
    }

    @Override
    public Observable<FilmsResponse> getFilmsObservable() {
        return service.getFilms()
                .doOnError(createErrorLogAction())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Bitmap> getVideoPreview(final String videoPath) {
        return Observable.create(new Observable.OnSubscribe<Bitmap>() {
            @Override
            public void call(Subscriber<? super Bitmap> subscriber) {
                Bitmap bitmap = null;
                MediaMetadataRetriever mediaMetadataRetriever = null;
                try {
                    mediaMetadataRetriever = new MediaMetadataRetriever();
                    mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
                    bitmap = mediaMetadataRetriever.getFrameAtTime();
                } catch (Exception e) {
                    subscriber.onError(e);
                } finally {
                    if (mediaMetadataRetriever != null)
                        mediaMetadataRetriever.release();
                }
                subscriber.onNext(bitmap);
                subscriber.onCompleted();
            }
        })
                .doOnError(createErrorLogAction())
                .onErrorReturn(new Func1<Throwable, Bitmap>() {
                    @Override
                    public Bitmap call(Throwable throwable) {
                        return null;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Action1<Throwable> createErrorLogAction() {
        return new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                Log.e(TAG, "Error while making request to net", throwable);
            }
        };
    }
}
