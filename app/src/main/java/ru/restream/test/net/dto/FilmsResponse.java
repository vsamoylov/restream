package ru.restream.test.net.dto;

import java.util.List;

/**
 * Created by HP on 27.06.2016.
 */
public class FilmsResponse {

    public int page;
    public List<Film> results;
    public int total_pages;
    public int total_results;
}
